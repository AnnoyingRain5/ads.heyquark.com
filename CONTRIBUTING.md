## requirements for new ads
Awesome! You'll need to submit a pull request to get your files in. There's a few
requirements:
- All your ads must be in a folder specific and unique to you - use your username
or something. All folders must be under /_ads. For example, mine's /_ads/quarky.
- Your PR can only change files under your folder in /_ads. If you want to do
anything else, like backend work, submit a seperate PR for those changes.
- All ads are single HTML/CSS files. You should embed all your stylesheets and
graphics into that file
- Prefer vector graphics over bitmaps, and readable XML over base64 blobs.
- Keep in mind that your ad will run through Jekyll, so you can and totally
should add a YAML frontmatter and take advantage of scss and loops and whatever.
- Your ad should look good in a viewport of anywhere between 250x190 and
150x150.
- Webmasters have been instructed to embed with sandboxed iframes, so that means
no JavaScript.
- Keep it simple!

A few legal bits:
- I reserve the right to modify or remove your ad if needed.
- I might not accept your PR. I probably will, but I reserve the right not to.
- I can elect others as collaborators who also get these controls over ads.
- Please just be nice! This is a hobby project that I'm opening up so my friends
can try it too. You can use this for whatever, but don't take it too seriously.

## contributing to the backend
This site uses Jekyll to process all the ads, and one Netlify/AWS function to
serve one at random. The AWS bit is under /dynamic, and it also runs through
Jekyll. You can see the processed version that actually runs on AWS
[here](https://ads.heyquark.com/dynamic/heyquark.js).
In terms of contributing, feel free to PR changes or open issues and we'll sort
stuff out on a case-by-case basis. Have fun!
