## heyquark ads
a *very* simple ad network

This is a small experiment in serving ads. You can read more about it
[here](https://ads.heyquark.com) or learn how to add your own ad in
CONTRIBUTING.md.

This repo runs on Netlify, and is live on
[ads.heyquark.com](https://ads.heyquark.com) right now!
